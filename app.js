'use strict';

/**
 * REQUIRE
 */
const CONFIG = require("./config");
process.env.CONFIG = JSON.stringify(CONFIG);
const express = require("express");
const http = require("http");
const path = require("path");
const bodyParser = require('body-parser');
const IOController = require("./app/controllers/io.controller.js");

/**
 * ROUTES
 */
const defaultRoute = require("./app/routes/default.route");
const presentationRoute = require("./app/routes/presentation.route");
const contentRoute = require("./app/routes/content.route");
const loginRoute = require("./app/routes/login.route");



const app = express();
app.use(bodyParser.json());
app.use(defaultRoute);
app.use(presentationRoute);
app.use(contentRoute);
app.use(loginRoute);
app.use("/admin", express.static(path.join(__dirname, "public/admin")));
app.use("/watch", express.static(path.join(__dirname, "public/watch")));

let server = http.createServer(app);
IOController.listen(server);

server.listen(CONFIG.port);