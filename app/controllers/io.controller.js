module.exports = this;

const ContentModel = require('./../models/content.model');

let maMap = new Map();

this.listen = (server) => {
    const io = require('socket.io')(server);
    io.on('connect', function (socket) {
        console.log(socket.id);
        socket.on('disconnect', function () {
            console.log('user disconnected');
        });

        socket.on('data_comm', function(socketId) {
            console.log("data comm " + socketId);
            maMap.set(socketId, socket);
        });

        socket.on('slidEvent', function (event) {
            let jsonEvent = JSON.parse(event);
            if (jsonEvent.CMD === ("START" || "END" || "BEGIN" || "PREV" || "NEXT")) {
                ContentModel.read(jsonEvent.PRESID, (err, data) => {
                    console.log(data.toJson());
                    socket.broadcast.emit('currentSlidEvent', data.toJson());
                });
            }
        });

        socket.on('broadcast', function(data) {
            console.log(socket.id + " : " + data);
        });
    });
};