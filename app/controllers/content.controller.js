"use strict";
const CONFIG = JSON.parse(process.env.CONFIG);
const ContentModel = require("./../models/content.model");
const fs = require("fs");
const path = require("path");

this.create = (req, res) => {
    let type = JSON.parse(JSON.stringify(req.body.type));
    let title = JSON.parse(JSON.stringify(req.body.title));
    let fileName = JSON.parse(JSON.stringify(req.body.fileName));
    let id = fileName;
    fileName += ".txt";
    let src = JSON.parse(JSON.stringify(req.body.src));

    let content = new ContentModel({type, id, title, src, fileName});

    ContentModel.create(content, (err) => {
        if (err) {
            console.log(err.message);
            res.status(500)
                .send("Error create file");
        }

        res.status(200)
            .send("Files created !");
    })
};

this.read = (req, res) => {
    let id = req.params.id;

    ContentModel.read(id, (err, data) => {
        if (err) {
            console.log(err.message);
            res.status(500);
            res.send("Error read file");
        }

        res.status(200)
            .send(data);
    });
};

this.list = (req, res) => {
    let jsonFiles = [];
    const jsonFormat = ".json";
    let parsedData = {};
    //Get all JSON files in the CONFIG.presentationDirectory
    fs.readdir(CONFIG.directory.content, (err, data) => {

        if (err) {
            res.send("No data");
        }
        if (data) {
            data.forEach(fileName => {
                if (path.extname(fileName) === jsonFormat) {
                    jsonFiles.push(fileName);
                }
            });

            jsonFiles.forEach(file => {
                // Get content from file
                fs.readFile(CONFIG.directory.content + "/" + file, (err, data) => {

                    if (err) {
                        res.status(500);
                        res.send("error");
                    }

                    // Define to JSON type
                    let content = JSON.parse(data);
                    parsedData[content.id] = content;

                    if (file == jsonFiles[jsonFiles.length - 1]) {
                        //Return data
                        res.status(200);
                        res.send(parsedData);
                    }
                });
            });
        } else {
            res.send("No data this directory : " + CONFIG.directory.content);
        }
        
    });
};

module.exports = this;