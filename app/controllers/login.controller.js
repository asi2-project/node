"use strict";
const request = require("request");

this.connect = (req, res) => {
    console.log(req.body);

    let options = {
        method: 'POST',
        url: 'http://127.0.0.1:8080/FrontAuthWatcherWebService/rest/WatcherAuth',
        headers: {
            'Content-type': 'application/json'
        },
        json: req.body
    };

    function callback(error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log(body);
            res.status(200)
                .send(body);
        } else {
            res.status(500)
                .send("Error during login");
        }
    }

    request(options, callback);
};



module.exports = this;