"use strict";

let multer = require("multer");
let express = require("express");
let contentController = require('./../controllers/content.controller');
let router = express.Router();
module.exports = router;

let multerMiddleware = multer({ "dest": "/tmp/" });

router.route("/contents/:id").get(contentController.read);
router.post("/contents", multerMiddleware.single("file"), contentController.create);
router.route("/contents").get(contentController.list);
/**
 * Multer ajoute à l'objet `request` la propriété `file` qui contient plusieurs informations comme:
 *  - request.file.path : le chemin d'acces du fichier sur le serveur
 *  - request.file.originalname : le nom original du fichier
 *  - request.file.mimetype : le type mime
 *  - ...
 */
