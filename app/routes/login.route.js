"use strict";

const express = require("express");
const router = express.Router();
const loginController = require("./../controllers/login.controller");

module.exports = router;

router.route("/login")
    .post(function (request, response) {
        loginController.connect(request, response);
    });