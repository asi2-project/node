'use strict';

var express = require("express");
const fs = require('fs');
var router = express.Router();
var CONFIG = JSON.parse(process.env.CONFIG);
var utils = require("../utils/utils");

module.exports = router;

router.route("/loadPres")
    .get(function (request, response) {
        utils.listFile(CONFIG.directory.presentation, ".json", function (err, data) {
            if (err) {
                console.error(err.message);
                response.status(500).send("Error");
            } else {
                let result = {};
                let cpt = 0;
                let dataLength = data.length;
                data.forEach(fileName => {
                    fs.readFile(CONFIG.directory.presentation + "/" + fileName, (err, data) => {
                        if (err) {
                            console.error(err.message);
                            response.status(500).send("Error");
                        }
                        let content = JSON.parse(data);
                        result[content.id] = content;
                        cpt ++;
                        if(cpt == dataLength) {
                            response.status(200).send(result);
                        }
                    });

                });
            }
        })
    });

router.route("/savePres")
    .post(function (request, response) {
        let content = request.body;
        console.log(content);
        let id = content.id;

        fs.writeFile(CONFIG.directory.presentation + "/" + id + ".json", JSON.stringify(content), (err) => {
            if (err) {
                throw err;
            }
            console.log('The file has been saved!');
            response.status(200).send("The file has been saved!");
        });
    });
