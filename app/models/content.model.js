'use strict';
const fs = require('fs');
const utils = require('../utils/utils');
const CONFIG = JSON.parse(process.env.CONFIG);

class ContentModel {
    constructor({type, id, title, src, fileName} = {}) {
        this.type = type;
        this.id = id;
        this.title = title;
        this.src = src;
        this.fileName = fileName;
    }

    getData() {
        return this.data;
    }

    setData(data) {
        this.data = data;
    }

    toJson() {
        let {type, id, title, src, fileName, data} = this;
        return {type, id, title, src, fileName, data};
    }

    static create(content, callback) {
        if (content) {
            if (content instanceof ContentModel) {
                if (!content.id) {
                    return callback("Id is null");
                }
                else {
                    if (content.type == "img" && content.fileName) {

                        let data = "";

                        if (content.getData()) {
                            data = content.getData();
                        }

                        fs.writeFile(utils.getDataFilePath(content.fileName.toString()).toString(), data, (err) => {
                            if (err) {
                                return callback(err);
                            }
                            else {
                                console.log('The file has been saved!');
                            }
                        });
                    }

                    fs.writeFile(utils.getMetaFilePath(content.id.toString()).toString(), JSON.stringify(content), 'utf8', (err) => {
                        if (err) {
                            return callback(err);
                        }
                        else {
                            console.log('The metadata has been saved!');
                            callback();
                        }
                    });
                }
            }
            else {
                return callback("Content is not an instance of ContentModel.\"");
            }
        }
        else {
            return callback("Content is null.");
        }
    }

    static read(id, callback) {
        if (id == null) {
            callback(new Error("ID must be not null"), null);
        } else {
            fs.readFile(utils.getMetaFilePath(id), 'utf8', (err, data) => {
                if (err) {
                    callback(err, null);
                } else {
                    let c = new ContentModel(JSON.parse(data));
                    c.setData(JSON.parse(data).data);
                    callback(null, c);
                }
            });
        }
    }

    static update(content, callback) {
        if (content) {
            if (content.id) {
                utils.fileExists(utils.getDataFilePath(content.fileName.toString()).toString(), (err) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        if (content.type == "img" && content.data.length > 0) {

                            let data = content.getData();
                            fs.writeFile(utils.getDataFilePath(content.fileName.toString()).toString(), data, (err) => {
                                if (err) {
                                    return callback(err, null);
                                } else {
                                    console.log('The file has been updated!');
                                }
                            });
                        }


                        fs.writeFile(utils.getMetaFilePath(content.id), JSON.stringify(content), (err) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, "OK");
                            }
                        });
                    }
                });
            } else {
                callback(new Error("Id must be not null"), null);
            }
        } else {
            callback(new Error("Content must be not null"), null);
        }
    }

    static delete(id, callback) {
        if (id) {

            let metaFilePath = utils.getMetaFilePath(id.toString());
            let fileNamePath = utils.getDataFilePath(id.toString());
            utils.fileExists(metaFilePath.toString(), (err) => {
                if (err) {
                    callback(new Error("File needs to exists"), null);
                } else {

                    fs.unlink(metaFilePath, (err) => {
                        if (err) {
                            callback(err, null);
                        }
                    });
                }
            });

            utils.fileExists(fileNamePath.toString() + ".txt", (err) => {
                if (err) {
                    callback(new Error("File needs to exists"), null);
                } else {

                    fs.unlink(fileNamePath + ".txt", (err) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, "OK");
                        }
                    });
                }
            });

        } else {
            callback(new Error("ID must be not null"), null);
        }


    }
}


module.exports = ContentModel;